import express, { Request, Response } from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'

import usersRoutes from './routes/user.routes'
import entryRoutes from './routes/entry.routes'
import pageRoutes from './routes/page.routes'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

app.use('/u', usersRoutes)
app.use('/e', entryRoutes)
app.use('/p', pageRoutes)

app.get('/', async (req: Request, res: Response) => {
  res.status(200).json({ message: 'Hello World!' })
})

export default app
