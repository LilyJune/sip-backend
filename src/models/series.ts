import { Model, Sequelize, DataTypes } from 'sequelize'

export default class Series extends Model {
  public id?: number
  public title!: string
  public description!: string
}

export const SeriesMap = async (sequelize: Sequelize): Promise<void> => {
  Series.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING(255)
    },
    description: {
      type: DataTypes.TEXT
    }
  }, {
    sequelize,
    tableName: 'series',
    timestamps: false
  })
  await Series.sync()
}
