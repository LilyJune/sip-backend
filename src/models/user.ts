import { Model, Sequelize, DataTypes } from 'sequelize'

export default class User extends Model {
  public id?: number
  public name!: string
  public username!: string
  public email!: string
  public birthdate!: Date
  public country?: string
}

export const UserMap = async (sequelize: Sequelize): Promise<void> => {
  User.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255)
    },
    username: {
      type: DataTypes.STRING(255)
    },
    email: {
      type: DataTypes.STRING(255)
    },
    birthdate: {
      type: DataTypes.DATE
    },
    country: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'users',
    timestamps: true
  })
  await User.sync()
}
