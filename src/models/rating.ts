import { Model, Sequelize, DataTypes } from 'sequelize'

export default class Ratings extends Model {
  public id?: number
  public user_id!: number
  public series_id!: number
  public rating!: number
}

export const RatingsMap = async (sequelize: Sequelize): Promise<void> => {
  Ratings.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER
    },
    series_id: {
      type: DataTypes.INTEGER
    },
    rating: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    tableName: 'ratings',
    timestamps: false
  })
  await Ratings.sync()
}
