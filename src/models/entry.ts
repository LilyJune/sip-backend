import { Model, Sequelize, DataTypes } from 'sequelize'

export default class Entry extends Model {
  public id?: number
  public title!: string
  public description!: string
  public group_id!: string
  public pages!: number
}

export const EntryMap = async (sequelize: Sequelize): Promise<void> => {
  Entry.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    title: {
      type: DataTypes.STRING(255)
    },
    description: {
      type: DataTypes.TEXT
    },
    group_id: {
      type: DataTypes.INTEGER
    },
    pages: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    tableName: 'entries',
    timestamps: true
  })
  await Entry.sync()
}
