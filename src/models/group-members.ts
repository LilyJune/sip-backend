import { Model, Sequelize, DataTypes } from 'sequelize'

export default class GroupMembers extends Model {
  public id?: number
  public user_id!: number
  public group_id!: number
}

export const GroupMembersMap = async (sequelize: Sequelize): Promise<void> => {
  GroupMembers.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER
    },
    group_id: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    tableName: 'groups_members',
    timestamps: true
  })
  await GroupMembers.sync()
}
