import { Model, Sequelize, DataTypes } from 'sequelize'

export default class Group extends Model {
  public id?: number
  public name!: string
  public description!: string
}

export const GroupMap = async (sequelize: Sequelize): Promise<void> => {
  Group.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255)
    },
    description: {
      type: DataTypes.TEXT
    }
  }, {
    sequelize,
    tableName: 'groups',
    timestamps: true
  })
  await Group.sync()
}
