import { Model, Sequelize, DataTypes } from 'sequelize'

export default class EntryRatings extends Model {
  public id?: number
  public user_id!: number
  public entry_id!: number
  public rating!: number
}

export const EntryRatingsMap = async (sequelize: Sequelize): Promise<void> => {
  EntryRatings.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER
    },
    entry_id: {
      type: DataTypes.INTEGER
    },
    rating: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    tableName: 'entry_ratings',
    timestamps: true
  })
  await EntryRatings.sync()
}
