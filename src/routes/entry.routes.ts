
import { Router, Request, Response } from 'express'
import Entry, { EntryMap } from '../models/entry'
import EntryRatings, { EntryRatingsMap } from '../models/entry-rating'
import database from '../database'

const router = Router()

router.get('/', async (req: Request, res: Response): Promise<void> => {
  await EntryMap(database)
  const result = await Entry.findAll()
  res.status(200).json(result)
})

router.get('/rating', async (req: Request, res: Response): Promise<void> => {
  await EntryMap(database)
  await EntryRatingsMap(database)

  EntryRatings.belongsTo(Entry, { foreignKey: 'entry_id' })
  Entry.hasMany(EntryRatings, { foreignKey: 'entry_id' })
  const result = await Entry.findAll({ include: [EntryRatings] })
  res.status(200).json(result)
})

router.get('/:id', async (req: Request, res: Response): Promise<void> => {
  await EntryMap(database)
  const id = Number(req.params.id)
  const result = await Entry.findByPk(id)
  res.status(200).json(result)
})

router.get('/:id/rating', async (req: Request, res: Response): Promise<void> => {
  await EntryMap(database)
  await EntryRatingsMap(database)

  const id = Number(req.params.id)

  EntryRatings.belongsTo(Entry, { foreignKey: 'entry_id' })
  Entry.hasMany(EntryRatings, { foreignKey: 'entry_id' })
  const result = await Entry.findByPk(id, {
    include: [EntryRatings]
  })
  res.status(200).json(result)
})

export default router
