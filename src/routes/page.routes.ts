
import { Router, Request, Response } from 'express'
import fs from 'fs'

const router = Router()

router.get('/:id/:page', async (req: Request, res: Response): Promise<void> => {
  const id = String(req.params.id)
  const page = String(req.params.page)
  let absPath = String(__dirname)
  absPath = absPath.substring(0, absPath.lastIndexOf('\\'))
  absPath = absPath.substring(0, absPath.lastIndexOf('\\'))
  const path = `${absPath}\\storage\\${id}\\${page}.png`
  if (fs.existsSync(path)) {
    res.status(200).sendFile(path)
  } else {
    res.status(404).send({ message: 'File Not Found!' })
  }
})

export default router
