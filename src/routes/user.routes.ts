
import { Router, Request, Response } from 'express'
import User, { UserMap } from '../models/user'
import database from '../database'

const router = Router()

router.get('/', async (req: Request, res: Response): Promise<void> => {
  await UserMap(database)
  const result = await User.findAll()
  res.status(200).json(result)
})

router.get('/:id', async (req: Request, res: Response): Promise<void> => {
  await UserMap(database)
  const id = Number(req.params.id)
  const result = await User.findByPk(id)
  res.status(200).json(result)
})

router.post('/', async (req: Request, res: Response): Promise<void> => {
  let newUser = req.body as User
  await UserMap(database)
  const result = await User.create(newUser)
  newUser = result.getDataValue as unknown as User
  res.status(201).json(newUser)
})

export default router
