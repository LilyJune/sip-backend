import app from './app'
import { port } from './config'

app.listen(port, () => {
  console.log(`API started at http://localhost:${port}`)
})
