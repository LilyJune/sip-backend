import { dbHost, dbPort, dbName, dbUser, dbPassword } from './config'
import { Sequelize } from 'sequelize'

export default new Sequelize({
  dialect: 'postgres',
  host: dbHost,
  port: dbPort,
  database: dbName,
  username: dbUser,
  password: dbPassword,
  timezone: 'US/Central'
})
