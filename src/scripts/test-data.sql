INSERT INTO "users" ("id", "name", "username", "email", "birthdate", "country") VALUES
(1, 'Stephen', 'step', 'step@sip.com', '1983-05-06', 'USA'),
(2,	'Felipe', 'phil', 'phil@sip.com', '1998-12-16', 'Brazil'),
(3,	'Pasha', 'pas', 'pas@sip.com', '1986-01-07', 'Poland'),
(4,	'Richard', 'rich', 'rich@sip.com', '1982-06-25', 'France'),
(5,	'Lynda', 'lyn', 'lyn@sip.com', '1981-06-14', 'Canada'),
(6,	'Kelly', 'kel', 'kel@sip.com', '1995-10-04', 'United Kingdom');

INSERT INTO "entries" ("title", "description", "pages") VALUES
('test Title 1', 'this is the first test', 2),
('test Title 2', 'this is the second test', 2),
('test Title 3', 'this is the third test', 3),
('test Title 4', 'this is the fourth test', 4);

INSERT INTO "entry_ratings" ("user_id", "entry_id", "rating") VALUES
(1, 1, 10),
(1, 2, 9),
(2, 1, 7),
(3, 1, 4),
(6, 4, 8);
